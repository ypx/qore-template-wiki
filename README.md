# Qore Template - Wiki Documentation

## Demo site

https://qore-template-wiki.netlify.app/

## How to start

`yarn dev`

## How to deploy

just build and place the build folder (default is `/.next`) on your web directory or simply deploy to vercel / netlify
