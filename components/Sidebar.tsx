import { Divider, PageHeader, Tree, Typography } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import React from 'react'
import useList from '../hooks/useData'
import { DataNode } from 'antd/lib/tree'
import { useRouter } from 'next/router'

const Sidebar = () => {
  const router = useRouter()
  const { categories, articles, loading } = useList()
  const [tree, setTree] = React.useState<DataNode[]>([])
  React.useEffect(() => {
    if (loading) return
    const listTemp: any[] = categories.map(cat => ({
      title: cat.category.displayField,
      key: cat.category.id,
      type: 'category',
      parent: cat.parent?.id || null,
      isLeaf: false,
    }))
    listTemp.forEach((node, index) => {
      const categoryArticles = articles.data
        .filter(article => article.category.id === node.key)
        .sort((a, b) => a.order - b.order)
        .map(article => ({
          title: article.name,
          key: article.id,
          type: 'article'
        }))
        if (!listTemp[index].children) {
          Object.assign(listTemp[index], { children: categoryArticles })
        }
        if (!node.parent) return
        const parentIndex = categories.findIndex(el => el.category.id === node.parent)
        if (!listTemp[parentIndex].children) {
          return Object.assign(listTemp[parentIndex], { children: [node] })
        }
        listTemp[parentIndex].children.push(node)
    })
    setTree(listTemp.filter(node => !node.parent))
  }, [categories, loading])
  const onSelect = (selectedKeys: any, info: any) => {
    const { type } = info.node
    if (type === 'article') {
      router.push(`/article/${selectedKeys}`)
    } else {
      router.push(`/#${selectedKeys}`)
    }
  };
  return (
    <>
      <div className="fixed top-0 bg-white z-20 w-72">
        <PageHeader title={<Typography.Text type="secondary">Navigation</Typography.Text>} />
        <Divider className="m-0" />
      </div>
      <div className="px-4 pb-8 mt-24">
        <Tree
          className="bg-transparent"
          showLine
          switcherIcon={<DownOutlined />}
          defaultExpandedKeys={[]}
          onSelect={onSelect}
          treeData={tree}
        />
      </div>
    </>
  )
}

export default Sidebar
