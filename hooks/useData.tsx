import React from 'react'
import qoreContext from '../utils/qoreContext'

const useData = () => {
  const categories = qoreContext.views.categoryMapDefaultView.useListRow()
  const articles = qoreContext.views.articlesDefaultView.useListRow()
  const [loading, setLoading] = React.useState(true)
  React.useEffect(() => {
    categories.status === 'success' && articles.status === 'success' && setLoading(false)
  }, [categories, articles]);
  return {
    categories: categories.data.sort((a, b) => a.order - b.order),
    articles,
    loading
  }
}

export default useData
