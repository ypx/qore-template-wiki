import type { AppProps /*, AppContext */ } from 'next/app'
import dynamic from 'next/dynamic'
import '../styles/globals.css'
import { Layout } from 'antd'

const Sidebar = dynamic(() => import("../components/Sidebar"), { ssr: false });

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Layout className="h-screen">
      <Layout.Sider
        width={289}
        theme="light"
        style={{ borderRight: '1px solid #DDD' }}
        className="bg-cyan-800"
      >
        <Sidebar />
      </Layout.Sider>
      <Layout.Content style={{ overflow: 'scroll', background: 'white' }}>
        <Component {...pageProps} />
      </Layout.Content>
    </Layout>
  );
};

export default MyApp;
