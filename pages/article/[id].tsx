import { PageHeader, Divider, Typography, Skeleton } from 'antd'
import { useRouter } from 'next/router'
import React from 'react'
import qoreContext from '../../utils/qoreContext'

const article = () => {
  const router = useRouter()
  const { id } = router.query
  const { data: [article], status } = qoreContext.views.articlesDefaultView.useListRow({
    id: id as string
  })
  return (
    <div>
      <div className="fixed top-0 bg-white w-full z-10">
        <PageHeader
          title={
            <Typography.Text type="secondary">
              {article?.name || 'N/A'}
            </Typography.Text>}
          onBack={router.back}
        />
        <Divider className="m-0" />
      </div>
      <div className="px-6 pb-8 mt-24">
        {status !== 'success' ?
          <Skeleton /> :
          <>
            <p>{article?.description || 'N/A'}</p>
            <div dangerouslySetInnerHTML={{__html: article?.contents || '<div>N/A</div>'}} />
          </>
        }
      </div>
    </div>
  )
}

export default article
