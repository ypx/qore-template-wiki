import { Divider, PageHeader, Skeleton, Typography } from 'antd'
import React from 'react'
import useList from '../hooks/useData'
import Articles from '../components/Articles'

const contents = () => {
  const { categories, articles, loading } = useList()
  const [list, setList] = React.useState<{[key:string]: any}[]>([])
  React.useEffect(() => {
    if (loading) return
    const listTemp: any[] = categories.map(cat => ({
      ...cat.category,
      description: cat.description,
      parent: cat.parent?.id || null,
    }))
    listTemp.forEach((node, index) => {
      const categoryArticles = articles.data.filter(article => article.category.id === node.id).sort((a, b) => a.order - b.order)
      Object.assign(listTemp[index], { articles: categoryArticles })
      if (!node.parent) return
      const parentIndex = categories.findIndex(el => el.category.id === node.parent)
      if (!listTemp[parentIndex].children) {
        return Object.assign(listTemp[parentIndex], { children: [node] })
      }
      
      listTemp[parentIndex].children.push(node)
    })
    setList(listTemp.filter(node => !node.parent))
  }, [categories, loading])
  const CategoriesList = (props: { list: any[] }) => {
    return (
      <div>
        {props.list.map(node => <Item key={node.id} node={node} />)}
      </div>
    )
  }
  const Item = (props: { node: any }) => {
    return (
      <div className="mt-8">
        <Typography.Title id={props.node.id} level={4}>
          {props.node.displayField}
        </Typography.Title>
        <Divider />
        <p className="mb-8">{props.node.description}</p>
        <div>
          {props.node.articles && props.node.articles.length > 0 && (
            <Articles list={props.node.articles} />
          )} 
          {props.node.children && props.node.children.length > 0 && (
            <CategoriesList list={props.node.children} />
          )}
        </div>
      </div>
    )
  }
  return (
    <div>
      <div className="fixed top-0 bg-white w-full z-10">
        <PageHeader title={<Typography.Text type="secondary">Table of Contents</Typography.Text>} />
        <Divider className="m-0" />
      </div>
      <div className="px-6 pb-8 mt-24">
        {loading && <Skeleton />}
        {list && <CategoriesList list={list} />}
      </div>
    </div>
  )
}

export default contents