import React from 'react'
import Contents from './contents'

export default function Home() {
  return <Contents />
}
