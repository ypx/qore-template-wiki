// [WARNING] This file is generated by running `$ qore codegen` on your root project, please do not edit

type ArticlesTableRow = {
  id: string;
  name: string;
  description: string;
  contents: string;
  category: CategoriesTableRow;
  order: number;
  icon: string;
};

type CategoriesTableRow = {
  id: string;
  name: string;
  description: string;
  categoryMap: { nodes: CategoryMapTableRow[] };
  categoryMap1: { nodes: CategoryMapTableRow[] };
  articles: { nodes: ArticlesTableRow[] };
};

type CategoryMapTableRow = {
  id: string;
  category: { id: string; displayField: string };
  parent: { id: string; displayField: string };
  description: string;
  order: number;
};

type ArticlesDefaultViewViewRow = {
  read: {
    id: string;
    name: string;
    description: string;
    contents: string;
    category: CategoriesTableRow;
    order: number;
    icon: string;
  };
  write: {
    name: string;
    description: string;
    contents: string;
    category: string[];
    order: number;
    icon: string;
  };
  params: {
    id?: string;
  };
  actions: {};
};

type CategoriesDefaultViewViewRow = {
  read: {
    id: string;
    name: string;
    description: string;
  };
  write: {
    name: string;
    description: string;
  };
  params: {};
  actions: {};
};

type CategoryMapDefaultViewViewRow = {
  read: {
    id: string;
    category: { id: string; displayField: string };
    parent: { id: string; displayField: string };
    description: string;
    order: number;
  };
  write: {
    category: string[];
    parent: string[];
    order: number;
  };
  params: {};
  actions: {};
};

type MemberDefaultViewViewRow = {
  read: {
    id: string;
    email: string;
    role: { id: string; displayField: string };
  };
  write: {
    email: string;
  };
  params: {};
  actions: {};
};

export type QoreProjectSchema = {
  articlesDefaultView: ArticlesDefaultViewViewRow;
  categoriesDefaultView: CategoriesDefaultViewViewRow;
  categoryMapDefaultView: CategoryMapDefaultViewViewRow;
  memberDefaultView: MemberDefaultViewViewRow;
};
